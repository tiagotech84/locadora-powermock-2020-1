package br.ucsal.testequalidade20192.locadora;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import br.ucsal.testequalidade20192.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20192.locadora.dominio.Cliente;
import br.ucsal.testequalidade20192.locadora.dominio.Locacao;
import br.ucsal.testequalidade20192.locadora.dominio.Modelo;
import br.ucsal.testequalidade20192.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20192.locadora.dominio.enums.SituacaoVeiculoEnum;
import br.ucsal.testequalidade20192.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20192.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20192.locadora.persistence.VeiculoDAO;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ClienteDAO.class,LocacaoDAO.class, VeiculoDAO.class, LocacaoBO.class })
public class LocacaoBOUnitarioTest {

	/**
	 * Verificar se ao locar um veículo disponível para um cliente cadastrado, um
	 * contrato de locação é inserido.
	 * 
	 * Método:
	 * 
	 * public static Integer locarVeiculos(String cpfCliente, List<String> placas,
	 * Date dataLocacao, Integer quantidadeDiasLocacao) throws
	 * ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
	 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado
	 *
	 * Observação1: lembre-se de mocar os métodos necessários nas classes
	 * ClienteDAO, VeiculoDAO e LocacaoDAO.
	 * 
	 * Observação2: lembre-se de que o método locarVeiculos é um método command.
	 * 
	 * @throws Exception
	 */
	
	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		
		Date dataLocacao = new Date();
		Integer quantidadeDiasLocacao = 5;
		LocacaoBO lb;
		
		String placa = "abc-1234";
		Veiculo veiculo = new Veiculo(placa, 2020, new Modelo("Palio"), 40.10);
		veiculo.setSituacao(SituacaoVeiculoEnum.DISPONIVEL);
		List<String> placas = Arrays.asList(placa);
		List<Veiculo> veiculos = Arrays.asList(veiculo);
		
		String cpfCliente = "1234567890";
		Cliente cliente = new Cliente(cpfCliente, "Tiago", "12312312");
		
		PowerMockito.mockStatic(ClienteDAO.class);
		PowerMockito.when(ClienteDAO.obterPorCpf(cpfCliente)).thenReturn(cliente);
		
		PowerMockito.mockStatic(VeiculoDAO.class);
		PowerMockito.when(VeiculoDAO.obterPorPlaca(placa)).thenReturn(veiculo);
		
		PowerMockito.mockStatic(LocacaoDAO.class);
		
		Integer numeroContratoEsperado = 2;
		LocacaoBO.locarVeiculos(cpfCliente, placas, dataLocacao, quantidadeDiasLocacao);
		Locacao locacao = new Locacao(cliente, veiculos, dataLocacao, quantidadeDiasLocacao);
		Integer numeroContratoAtual = locacao.getNumeroContrato();
		
		Assert.assertEquals(numeroContratoEsperado, numeroContratoAtual);
		
		PowerMockito.whenNew(Locacao.class).withAnyArguments().thenReturn(locacao);
		
		Whitebox.setInternalState(locacao, "numeroContrato", numeroContratoEsperado);;
	}
}
